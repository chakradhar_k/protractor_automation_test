// An example configuration file.
exports.config = {
  directConnect: true,
  ignoreSynchronization : true,

  // Capabilities to be passed to the webdriver instance.
  capabilities: {
    'browserName': 'chrome'
  },

  // Framework to use. Jasmine is recommended.
  framework: 'jasmine',

  // Spec patterns are relative to the current working directory when
  // protractor is called.
 //specs: ['ECOMP_Angular_UI.js'],
 //specs: ['Protractor_Automation.js'],
  specs: ['example_spec.js'],

  // Options to be passed to Jasmine.
  jasmineNodeOpts: {
    defaultTimeoutInterval: 300000
  }
};
