describe('Ecomp Loginpage', function() {
	
	
  it('should show the credentials page', function() {
	  
	  beforeEach(function(){
	  browser.ignoreSynchronization = true;
	  });
	  
	   //Enter into the ECOMP UI credential page
    browser.driver.get('https://mtsnjv9nspa01.aic.cip.att.com:8443/ecomp/login.htm');
    browser.driver.sleep(1000);
	var loginPortalLink = browser.driver.findElement(By.linkText('Click here to login'));
	loginPortalLink.click();
	browser.driver.sleep(1000);
	
	   // Enter Login credentials
      var userId = browser.driver.findElement(By.name('userid'));
	  var password = browser.driver.findElement(By.name('password'));
	  userId.sendKeys('ma987d');
	  password.sendKeys('hello468$');
	  var okButton = browser.driver.findElement(By.name('btnSubmit')); 
	  okButton.click();
	  browser.driver.sleep(1000);
	  
	  //Click on Success - OK Button
	  var succOKBtn = browser.driver.findElement(By.name('successOK')); 
	  succOKBtn.click();
	  browser.driver.sleep(1000);
	  
	  });
	  
	  //Perform operations on Angular page - Create Policy
       it('Create a policy', function() {
		   
		  var EC = protractor.ExpectedConditions;
		  
		  //Click on Policy Dropdown
		  
		    var policyDropdown = element(by.xpath("(.//span[contains(text(),'Policy')])[1]"));
			
			 browser.wait(EC.visibilityOf(policyDropdown), 2000).then(function(){
			 policyDropdown.click();
			 browser.sleep(2000);
			 
			 // Click on Editor Link under policy
		    var editorLink = element(by.xpath(".//*[text()='Editor']"));
			editorLink.click();
			browser.sleep(15000);
			
			//Search for the existing scope
			var scopeSearchBar = element(by.xpath("(.//*[contains(text(),'Policy Editor')])[1]/following::input[1]"));
			expect(scopeSearchBar.isEnabled()).toBe(true);
			scopeSearchBar.click();
			scopeSearchBar.sendKeys("Automation_Ecomp");
			
		    browser.sleep(60000);
			
			//Create a new policy under the selected scope
			browser.actions().mouseMove(element(by.xpath(".//a[contains(@title,'Automation_Ecomp')]"))).perform();
            browser.actions().click(protractor.Button.RIGHT).perform().then(function () {
            browser.sleep(2000);
			
			
            var craetePolicyContextClick = element(by.xpath(".//*[@id='context-menu']/ul/li[2]/a"));
            browser.wait(EC.visibilityOf(craetePolicyContextClick), 2000).then(function(){
            craetePolicyContextClick.click();
			browser.sleep(2000);
			 
			// Select the policy type
			var policyTypeDropdown = element(by.xpath(".//*[contains(@value,'Action')]"));
			policyTypeDropdown.click(); 
			browser.sleep(1000);
			
			//Enter the policy name
			var policyName = element(by.model("temp.policy.policyName"));
			policyName.sendKeys("Auto_Policy1");
			browser.sleep(1000);
			
			//Enter the policy description
			var policyDescription = element(by.model("temp.policy.policyDescription"));
			policyDescription.sendKeys("Description_for_Auto_Policy1");
			browser.sleep(1000);
			});
			
			//Add dropdown value under rule algorithm
			var addRuleAlgorithmButton = element(by.xpath(".//*[@id='createNewPolicy']/div/div/div[2]/div/div[1]/div[4]/form/div/div/button"));
			addRuleAlgorithmButton.click(); 
			browser.sleep(1000);
			
		    var ruleA1Dropdown = element(by.xpath("(.//*[@value='boolean'])[1]"));
			ruleA1Dropdown.click(); 
			browser.sleep(3000);
			
			var ruleA1Dropdown2 = element(by.xpath(".//*[@id='createNewPolicy']/div/div/div[2]/div/div[1]/div[4]/form/div[2]/div[3]/select/option[2]"));
			ruleA1Dropdown2.click();
			browser.sleep(3000);
			
			
			
			var ruleAlgorithmDescription = element(by.model("ruleAlgorithmschoice.dynamicRuleAlgorithmField2"));
			ruleAlgorithmDescription.sendKeys("Description_for_Auto_Policy_Dropdown");
			browser.sleep(1000);
			
			
			//Select the action performer
			var actionPerformer = element(by.xpath("(.//*[@value='PEP'])[1]"));
			actionPerformer.click(); 
			browser.sleep(2000);
			
			//Select the action attribute
			var actionAttribute = element(by.xpath(".//*[@id='createNewPolicy']/div/div/div[2]/div/div[1]/div[6]/select/option[2]"));
			actionAttribute.click();
			browser.sleep(2000);
			
			//Click on VALIDATE button
			var validateButton = element(by.xpath("(.//*[text()='Validate'])"));
			validateButton.click();
			expect(validateButton.getText()).toEqual('Validate');
			browser.sleep(3000);
			
			//Click on SAVE button
			var saveButton = element(by.xpath("(.//*[text()='Save'])[2]"));
			saveButton.click();
			browser.sleep(2000);
			
			//Close the sub-window
			var closeButton = element(by.xpath("(.//*[text()='Close'])[20]"));
			closeButton.click();
			browser.sleep(3000);
			
});
			
			
});
		
			
});
   

});
